import React from 'react'
import {
    ExecuteConfig,
    GetEvent,
    GetPlayer,
    ListPlayersOfEvent,
    MonitorExecution,
    UpdatePlayer
} from "../../../helper/api";


export default class Play extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            ready: false,
            players: [],
            me: [],
            event: null,
            eventId: null,
            playerId: null,
            trials: []
        }

    }

    componentDidMount() {

        if (typeof window !== 'undefined') {
            const [_, eventId, playerId] = window.location.pathname.match(/\/([0-9a-f]*)\/([0-9a-f]*)/);

            this.refreshEventInfo(eventId, playerId);

            this.setState({
                eventId: eventId,
                playerId: playerId,
                trials: [<Trial key={0} playerId={playerId} init={{}}/>]
            })
        }


    }

    refreshEventInfo(eventId = this.state.eventId, playerId = this.state.playerId) {
        if (document.hidden) return setTimeout(() => this.refreshEventInfo(eventId, playerId), 12000);

        Promise.all([
            ListPlayersOfEvent(eventId).then(({players}) => this.setState({players})),
            GetEvent(eventId).then(event => this.setState({event})),
            GetPlayer(playerId).then(player => this.setState({me: player}))
        ]).then(() => {
            this.setState({
                ready: true
            })
        }).catch(e => {
            console.error("Error refreshing event info", e);
        }).finally(() => {
            setTimeout(() => this.refreshEventInfo(eventId, playerId), 12000);
        })
    }

    render() {
        if (!this.state.ready) return 'Loading...'

        return <div className={"playZone"}>
            <div className={"play-head"}>
                <div className={"welcome"}>
                    Welcome, {this.state.me.playerName}!
                </div>
                <div className={"flex-space"}/>
                <div className={"other-players"}>
                    {this.state.players.map((p, i) => (<PlayerAvatar key={i} name={p.playerName}/>))}
                </div>
            </div>
            <div className={"trials-wrap"}>
                {this.state.trials}
            </div>

        </div>
    }
}

function PlayerAvatar({name}) {
    return <div className={"player-avatar"}>
        {name}
    </div>
}

const SIMUL = 30;

class Trial extends React.Component {
    constructor(props) {
        super(props);
        this.state = Object.assign({
            config: {
                pieceEvaluation: {
                    'q': 0,
                    'b': 0,
                    'n': 0,
                    'r': 0,
                    'p': 0,
                    'k': 0
                },
                checkWeight: 0,
                checkmateWeight: 0

            },
            batchId: null,
            result: null,
            stage: 0
        }, props.init);
    }

    updateConfig(path, val) {
        if (isNaN(parseFloat(val))) return;
        if (parseInt(val) === parseFloat(val)) val = parseInt(val);
        else val = parseFloat(val);

        path = path.split('.')
        let config = JSON.parse(JSON.stringify(this.state.config))
        let root = config;
        for (const prop of path.slice(0, -1)) {
            root = root[prop];
        }
        root[path[path.length - 1]] = val;
        this.setState({config: config})
    }

    getConfig(path) {
        path = path.split('.')
        let root = this.state.config;
        for (const prop of path) {
            root = root[prop];
        }
        return root;
    }

    renderEditor() {
        return <>
            <div className={"trial-row"}>
                <label className={'flex'}>Queen Evaluation:
                    <div className={'flex-space'}/>
                    <input type={'number'}
                           onChange={({target}) => this.updateConfig('pieceEvaluation.q', parseInt(target.value))}
                           value={this.getConfig('pieceEvaluation.q')}/></label>
            </div>
            <div className={"trial-row"}>
                <label className={'flex'}>Rook Evaluation:
                    <div className={'flex-space'}/>
                    <input type={'number'}
                           onChange={({target}) => this.updateConfig('pieceEvaluation.r', parseInt(target.value))}
                           value={this.getConfig('pieceEvaluation.r')}/></label>
            </div>
            <div className={"trial-row"}>
                <label className={'flex'}>Bishop Evaluation:
                    <div className={'flex-space'}/>
                    <input type={'number'}
                           onChange={({target}) => this.updateConfig('pieceEvaluation.b', parseInt(target.value))}
                           value={this.getConfig('pieceEvaluation.b')}/></label>
            </div>
            <div className={"trial-row"}>
                <label className={'flex'}>Knight Evaluation:
                    <div className={'flex-space'}/>
                    <input type={'number'}
                           onChange={({target}) => this.updateConfig('pieceEvaluation.n', parseInt(target.value))}
                           value={this.getConfig('pieceEvaluation.n')}/></label>
            </div>
            <div className={"trial-row"}>
                <label className={'flex'}>Pawn Evaluation:
                    <div className={'flex-space'}/>
                    <input type={'number'}
                           onChange={({target}) => this.updateConfig('pieceEvaluation.p', parseInt(target.value))}
                           value={this.getConfig('pieceEvaluation.p')}/></label>
            </div>
            <div className={"trial-row"}>
                <label className={'flex'}>King Evaluation:
                    <div className={'flex-space'}/>
                    <input type={'number'}
                           onChange={({target}) => this.updateConfig('pieceEvaluation.k', parseInt(target.value))}
                           value={this.getConfig('pieceEvaluation.k')}/></label>
            </div>
            <div className={"trial-row"}>
                <label className={'flex'}>Check weight:
                    <div className={'flex-space'}/>
                    <input type={'number'}
                           onChange={({target}) => this.updateConfig('checkWeight', parseInt(target.value))}
                           value={this.getConfig('checkWeight')}/></label>
            </div>
            <div className={"trial-row"}>
                <label className={'flex'}>Checkmate weight:
                    <div className={'flex-space'}/>
                    <input type={'number'}
                           onChange={({target}) => this.updateConfig('checkmateWeight', parseInt(target.value))}
                           value={this.getConfig('checkmateWeight')}/></label>
            </div>
        </>
    }

    async run() {
        this.setState({
            stage: 1
        })
        const {batchId} = await ExecuteConfig({
            trial: {
                evalFn: 'e1',
                params: this.state.config,
                depth: 0,
            },
            random: {
                evalFn: 'random'
            }
        }, [{'white': 'trial', 'black': 'random', 'count': SIMUL}]).catch(e => {
            console.error(e);
            alert('There was a problem');
            this.setState({
                stage: 0
            })
            throw e;
        })
        this.setState({
            stage: 2,
            batchId: batchId
        })

        this.initMonitorLoop(batchId);
    }

    async initMonitorLoop(batchId,) {
        if (this.mloop) clearTimeout(this.mloop);

        const result = await MonitorExecution(batchId);

        if (result.games.filter(x => x.status === 'done').length === SIMUL) {
            return this.setState({
                stage: 3,
                result: result
            })
        }

        console.log(result);
        try {
            this.setState({
                result: result
            })
        } catch (e) {
            console.error("Error monitoring execution", e);
        }

        this.mloop = setTimeout(() => this.initMonitorLoop(batchId), 2500);
    }

    renderResults() {
        if (this.state.stage === 0) {
            return <>
                <p>When ready, run your configuration in the cloud to compare with a random player.</p>
                <button onClick={() => this.run()}>Run</button>
            </>
        } else if (this.state.stage === 1) {
            return <>
                <p>Submitting {SIMUL} trials against Random to cloud...</p>
            </>
        } else if (this.state.stage === 2) {
            return <>
                <p>Waiting for finish... {this.renderResult()}</p>
            </>
        } else if (this.state.stage === 3) {
            return <>
                {this.renderResult()}
                <button onClick={() => this.run()}>Run again</button>

            </>
        }
    }

    renderResult() {
        if (!this.state.result?.games) return '';
        // Assumes we played white, check that assumption above...
        const wins = this.state.result.games.filter(x => x.result && x.result["1-0"]).length;
        const losses = this.state.result.games.filter(x => x.result && x.result["0-1"]).length;
        const draws = this.state.result.games.filter(x => x.result && x.result["1/2-1/2"]).length;
        const pending = this.state.result.games.filter(x => !x.result).length;
        const estElo = Math.round((400 * (wins + losses + draws) + 400 * (wins - losses)) / (wins + losses + draws))
        return <>
            Wins: {wins} | Losses: {losses} |
            draws: {draws} {pending ? ' | pending: ' + pending : ''} {pending < SIMUL ? '| Est. ELO: ' + estElo : ''} |
        </>
    }

    renderControls() {
        return <>
            <button onClick={() => this.submit()}>Submit</button>
        </>
    }

    async submit() {
        const update = await UpdatePlayer(this.props.playerId, {config: this.state.config})
            .then(() => alert('Saved succeeded!'))
            .catch(e => alert('Save failed'))
    }

    render() {
        return <div className={"trial"}>
            <div className={"trial-header"}>
                <h2>Chess Royale</h2>
            </div>
            <div className={"trial-edit"}>
                {this.renderEditor()}
            </div>
            <div className={"trial-results"}>
                {this.renderResults()} {this.renderControls()}
            </div>

        </div>
    }
}
