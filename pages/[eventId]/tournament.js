import React from 'react'
import {
    ExecuteConfig,
    GetEvent,
    GetPlayer,
    ListPlayersOfEvent,
    MonitorExecution,
    UpdatePlayer
} from "../../helper/api";


export default class Tournament extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ready: false,
            players: [],
            event: null,
            eventId: null,
            started: false
        }

    }

    componentDidMount() {

        if (typeof window !== 'undefined') {
            const [_, eventId] = window.location.pathname.match(/\/([0-9a-f]*)\//);

            this.refreshEventInfo(eventId);

            this.setState({
                eventId: eventId,
            })
        }


    }

    refreshEventInfo(eventId = this.state.eventId) {
        if (document.hidden) return setTimeout(() => this.refreshEventInfo(eventId), 12000);

        Promise.all([
            ListPlayersOfEvent(eventId, true).then(({players}) => this.setState({players})),
            GetEvent(eventId).then(event => this.setState({event})),
        ]).then(() => {
            this.setState({
                ready: true
            })
        }).catch(e => {
            console.error("Error refreshing event info", e);
        }).finally(() => {
            setTimeout(() => this.refreshEventInfo(eventId), 12000);
        })
    }

    render() {
        if (!this.state.ready) return 'Loading...'

        return <div className={"playZone"}>
            <div className={"play-head"}>
                <div className={"welcome"}>
                    Tournament
                </div>
                <div className={"flex-space"}/>
                <div className={"other-players"}>
                    {this.state.players.map((p, i) => (
                        <PlayerAvatar hasSubmitted={p.submissionInfo && p.submissionInfo.config} key={i}
                                      name={p.playerName}/>))}
                </div>
            </div>
            <div className={"trials-wrap"}>
                {this.state.started ? <Swiss players={this.state.players.filter(x => x.submissionInfo.config)}/> :
                    <button onClick={() => this.setState({started: true})}>Start</button>}
            </div>

        </div>
    }
}

function PlayerAvatar({name, hasSubmitted}) {
    return <div className={"player-avatar " + (hasSubmitted ? 'player-avatar--good' : '')}>
        {name}
    </div>
}

class Swiss extends React.Component {
    constructor(props) {
        super(props);

        if (props.players.length % 2 === 1) {
            props.players.push({
                playerId: 'random',
                playerName: 'random',
                submissionInfo: {config: {evalFn: 'random'}}
            })
        }

        this.state = {
            players: props.players,
            round: 0,
            rounds: 15,
            stage: -1,
            matchups: [],
            result: null,
            pairs: [],
            standings: props.players.map(p => ({playerName: p.playerName, playerId: p.playerId, score: 0, games: 0}))
        }
    }

    componentDidMount() {
        this.startRound();
    }

    async startRound() {
        if (this.state.round === this.state.rounds) return; // Done

        this.setState({
            round: this.state.round + 1
        })

        // 1. Make pairings
        let pairs = [];

        for (let i = 0; i < this.state.standings.length - 1; i += 2) {
            pairs.push({white: this.state.standings[i].playerId, black: this.state.standings[i + 1].playerId});
        }

        this.setState({
            stage: 0,
            pairs: pairs
        })
    }

    renderResult() {
        return <p>{this.state.summary || ''}</p>
    }

    handleResult(result) {
        // Let's see who won?
        let newStandings = JSON.parse(JSON.stringify(this.state.standings));
        let summary = '';
        for (const game of result.games) {
            const whitePlayer = newStandings.find(x => x.playerId === game.white);
            const blackPlayer = newStandings.find(x => x.playerId === game.black);
            if (game.result === '1-0') {
                whitePlayer.score += 2;
                summary += `${whitePlayer.playerName} beats ${blackPlayer.playerName}. `
            } else if (game.result === '0-1') {
                blackPlayer.score += 2;
                summary += `${black.playerName} beats ${whitePlayer.playerName}. `

            } else {
                summary += `${whitePlayer.playerName} draws with ${blackPlayer.playerName}. `

                whitePlayer.score += 1;
                blackPlayer.score += 1;
            }
            whitePlayer.games += 1;
            blackPlayer.games += 1;
        }
        this.setState({
            standings: newStandings,
            summary: summary
        })
    }

    async run() {
        this.setState({
            stage: 1
        })
        const {batchId} = await ExecuteConfig({
                ...Object.fromEntries(this.state.players.map(x => [x.playerId, {...x.submissionInfo.config, depth: 0}]))
            },
            [this.state.pairs]).catch(e => {
            console.error(e);
            alert('There was a problem');
            this.setState({
                stage: 0
            })
            throw e;
        })
        this.setState({
            stage: 2,
            batchId: batchId
        })

        this.initMonitorLoop(batchId);
    }

    async initMonitorLoop(batchId,) {
        if (this.mloop) clearTimeout(this.mloop);
        let result;
        try {
            result = await MonitorExecution(batchId);
        } catch (e) {
            console.error("Error monitoring execution", e);
            return setTimeout(() => this.initMonitorLoop(batchId), 2500);
        }

        if (result.games.filter(x => x.status === 'done').length === this.state.pairs.length) {
            this.setState({
                stage: 3,
                result: result
            })
            return this.handleResult(result)

        }

        console.log(result);
        this.setState({
            result: result
        })


        this.mloop = setTimeout(() => this.initMonitorLoop(batchId), 2500);
    }


    renderResults() {
        if (this.state.stage === 0) {
            return <>
                <p>Ready to play the next round!</p>
                <button onClick={() => this.run()}>Run</button>
            </>
        } else if (this.state.stage === 1) {
            return <>
                <p>Submitting games to cloud...</p>
            </>
        } else if (this.state.stage === 2) {
            return <>
                <p>Waiting for finish... </p>
            </>
        } else if (this.state.stage === 3) {
            return <>
                {this.renderResult()}
                <button onClick={() => this.startRound()}>All done!</button>

            </>
        }
    }

    render() {
        return <div className={"swiss"}>
            <h3>Round {this.state.round}</h3>
            {this.renderResults()}
            <h4>Standings</h4>
            <table>
                {this.state.standings.map((s, i) => <tr key={s.playerId}>
                    <td>#{i + 1}</td>
                    <td>{s.playerName}</td>
                    <td>{s.score} / {s.games}</td>
                </tr>)}
            </table>
        </div>;
    }
}
