# Notes

## API

1. [X] Create
   player: https://tl58z2668k.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_create-player?eventId=123&playerName=Jake&action=create
2. [X] Read all players of
   event: https://tl58z2668k.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_create-player?eventId=123&action=list&master=1
3. [X] Read
   player: https://tl58z2668k.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_create-player?playerId=d2977dda2ce64b06d869542cd83711e8abd6456599cf7bdc1b46a81e5f8d26ef9&action=read
4. [X] Update
   player: https://tl58z2668k.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_create-player?playerId=4a3b5adb93a67993e00e25733fd0d29e07af5b3785a54636ddc8cdb298ca740de&action=update&submissionInfo=eyJ0ZXN0IjoxfQ%3D%3D
5. [X] Create admin
   item: https://kouryj11va.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_admin-crud?action=create&eventName=testEvent
6. [X] Read admin
   item: https://kouryj11va.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_admin-crud?action=read&eventId=55af4b1e497eb9c5c7107228e5789a1962d28f423979f8ea30b24341c23098d2a
7. [X] Update admin
   item: https://kouryj11va.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_admin-crud?action=update&active=1&eventId=55af4b1e497eb9c5c7107228e5789a1962d28f423979f8ea30b24341c23098d2a&eventName=TestEvent2&adminInfo=eyJzdGFnZSI6IlRFU1QiLCJhIjozfQ%3D%3D
8. [X] List admin
   items: https://kouryj11va.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_admin-crud?action=list&master=1
9. [X] ConfigExecutor (invoked asynchronously)
10. [X] ConfigExecutorLauncher: https://7rl5c8jc2k.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE-ConfigExecutorLauncher?config=eyJyYW5kb20iOnsiZXZhbEZuIjoicmFuZG9tIn19&plan=W3siYmxhY2siOiJyYW5kb20iLCJ3aGl0ZSI6InJhbmRvbSIsImNvdW50Ijo1fV0
11. [X] ConfigExecutorMonitor: https://mmbjyixh3c.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE-executorMonitor?batchId=e8ef2bb0df0a50e4f1ba6f83fb9605a02d7b8383ac5cd45107ff14eb8bb1e03
    - This API should support receiving
      (1) a config file defining the relevant configs,
      (2) a list of requested pairings (this might include a full round of a Swiss tournament).
    - The API then launches a function for each of those pairings in parallel (up to some specified concurrency).
        - Those functions are all given a certain ID, which is returned to the user to poll.
    - Those functions log their results to a table, which we can poll via a third API.
        - Gosh, there's a lot of APIs... at least 11 for this project...

Well, we spent the evening building APIs. As I suppose could have been expected. Now we have 3.5 hours to
build the application itself. Let's see what we can do. 

This is a [Next.js](https://nextjs.org/) project bootstrapped
with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed
on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited
in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated
as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions
are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use
the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme)
from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
