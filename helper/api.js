/*

1. [X] Create
   player: https://tl58z2668k.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_create-player?eventId=123&playerName=Jake&action=create
2. [X] Read all players of
   event: https://tl58z2668k.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_create-player?eventId=123&action=list&master=1
3. [X] Read
   player: https://tl58z2668k.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_create-player?playerId=d2977dda2ce64b06d869542cd83711e8abd6456599cf7bdc1b46a81e5f8d26ef9&action=read
4. [X] Update
   player: https://tl58z2668k.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_create-player?playerId=4a3b5adb93a67993e00e25733fd0d29e07af5b3785a54636ddc8cdb298ca740de&action=update&submissionInfo=eyJ0ZXN0IjoxfQ%3D%3D
5. [X] Create admin
   item: https://kouryj11va.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_admin-crud?action=create&eventName=testEvent
6. [X] Read admin
   item: https://kouryj11va.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_admin-crud?action=read&eventId=55af4b1e497eb9c5c7107228e5789a1962d28f423979f8ea30b24341c23098d2a
7. [X] Update admin
   item: https://kouryj11va.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_admin-crud?action=update&active=1&eventId=55af4b1e497eb9c5c7107228e5789a1962d28f423979f8ea30b24341c23098d2a&eventName=TestEvent2&adminInfo=eyJzdGFnZSI6IlRFU1QiLCJhIjozfQ%3D%3D
8. [X] List admin
   items: https://kouryj11va.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_admin-crud?action=list&master=1
9. [X] ConfigExecutor (invoked asynchronously)
10. [X] ConfigExecutorLauncher: https://7rl5c8jc2k.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE-ConfigExecutorLauncher?config=eyJyYW5kb20iOnsiZXZhbEZuIjoicmFuZG9tIn19&plan=W3siYmxhY2siOiJyYW5kb20iLCJ3aGl0ZSI6InJhbmRvbSIsImNvdW50Ijo1fV0
11. [X] ConfigExecutorMonitor: https://mmbjyixh3c.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE-executorMonitor?batchId=e8ef2bb0df0a50e4f1ba6f83fb9605a02d7b8383ac5cd45107ff14eb8bb1e03

 */
const sleep = async (ms) => new Promise(res => setTimeout(res, ms));

const get = async function (url, params, options) {
    options = Object.assign({
        retries: 3, /* Hopefully this won't have too many bad effects */
        retryable: true,
        retryDelayMS: 2000
    }, 1);

    const fetchUrl = Object.keys(params).length
        ? url + '?' + Object.entries(params).map(([key, val]) => `${key}=${encodeURIComponent(val)}`).join('&')
        : url;
    try {
        return await fetch(fetchUrl).then(x => x.json()).then((data) => {
            if (data.error) throw data;
            return data;
        });
    } catch (e) {
        console.error(e);
        if (options.retries) {
            await sleep(options.retryDelayMS);
            return get(url, params, {...options, retries: options.retries - 1})
        }
    }
}

const API_BASES = {
    PLAYER: 'https://tl58z2668k.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_create-player',
    ADMIN: 'https://kouryj11va.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE_admin-crud',
    LAUNCH: 'https://7rl5c8jc2k.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE-ConfigExecutorLauncher',
    MONITOR: 'https://mmbjyixh3c.execute-api.us-east-1.amazonaws.com/default/CHESSROYALE-executorMonitor'
}

export async function CreatePlayer(eventId, playerName, playerEmail) {
    return get(API_BASES.PLAYER, {eventId, playerName, playerEmail, action: 'create'})
}

export async function GetPlayer(playerId) {
    return get(API_BASES.PLAYER, {playerId, action: 'read'})
}

export async function ListPlayersOfEvent(eventId, master = false) {
    return get(API_BASES.PLAYER, {eventId, master: master ? 1 : 0, action: 'list'}).then(x => {
        // Fix JSON of config
        x.players.forEach(y => {
            if (y.submissionInfo) {
                y.submissionInfo = JSON.parse(y.submissionInfo)
            }
        });
        return x;

    });
}

export async function UpdatePlayer(playerId, submissionInfo) {
    return get(API_BASES.PLAYER, {playerId, submissionInfo: btoa(JSON.stringify(submissionInfo)), action: 'update'})
}

export async function CreateEvent(eventName) {
    return get(API_BASES.ADMIN, {eventName, action: 'create'})
}

export async function GetEvent(eventId) {
    return get(API_BASES.ADMIN, {eventId, action: 'read'})
}

export async function ListEvents(master = false) {
    return get(API_BASES.ADMIN, {master: master ? 1 : 0, action: 'list'})
}

export async function UpdateEvent(eventId, adminInfo, eventName, active) {
    return get(API_BASES.ADMIN, {
        eventId,
        adminInfo: btoa(JSON.stringify(adminInfo)),
        eventName: eventName,
        active: active,
        action: 'update'
    })
}

export async function ExecuteConfig(config, plan) {
    return get(API_BASES.LAUNCH, {config: btoa(JSON.stringify(config)), plan: btoa(JSON.stringify(plan))});
}

export async function MonitorExecution(batchId) {
    return get(API_BASES.MONITOR, {batchId}).then(x => {
        // Fix JSON of config
        if (x.games) {
            x.games.forEach(g => {
                g.result = JSON.parse(g.result)
            })
        }
        return x;

    });
}
